## usage

## install

    $ cargo install --path .

or

    $ sudo cargo install --path . --root /usr/local

## usage

    $ nitro_get_totp   -- list all totp tokens
    $ nitro_get_totp <slot_name>  -- store totp code into clipboard

    $ nitro_get_hotp   -- list all hotp tokens
    $ nitro_get_hotp <slot_name>  -- store hotp code into clipboard

    $ nitro_get_password - List all programmed slots
    $ nitro_get_password <slot name> - copy password to clipboard


### example

    $ nitro_get_totp
    0 - GH
    2 - GL
    
	$ nitro_get_totp GL
    0 - GH - 914646

