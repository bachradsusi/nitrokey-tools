use std::os::raw::c_char;
use std::os::raw::c_int;

#[link(name = "nitrokey")]
extern "C" {
    pub fn NK_status() -> *const c_char;
    pub fn NK_login_auto() -> c_int;
//    fn NK_user_authenticate(user_password: *const c_char, user_temporary_password: *const c_char) -> i32;
    //uint8_t NK_get_user_retry_count();",
    pub fn NK_get_totp_slot_name(slot_number: u8) -> *const c_char;
    //     int NK_totp_set_time(uint64_t time);"
    pub fn NK_totp_set_time(time: u64) -> c_int;
    //     char * NK_get_totp_code(uint8_t slot_number, uint64_t challenge, \
    //        uint64_t last_totp_time, uint8_t last_interval);",
    pub fn NK_get_totp_code(slot_number: u8, challenge: u64,
                            last_totp_time: u64, last_interval: u8) -> *const c_char;

//     char * NK_get_totp_code_PIN(uint8_t slot_number, uint64_t challenge, \
//        uint64_t last_totp_time, uint8_t last_interval, \
    //        const char *user_temporary_password);",
    // NK_C_API int NK_user_authenticate(const char* user_password, const char* user_temporary_password);
    pub fn NK_user_authenticate(user_password: *const c_char, user_temporary_password: *const c_char) -> c_int;
    pub fn NK_enable_password_safe(user_pin: *const c_char) -> c_int;
    //     uint8_t * NK_get_password_safe_slot_status();"
    pub fn NK_get_password_safe_slot_status() -> *const i8;

    //     char *NK_get_password_safe_slot_name(uint8_t slot_number);",
    pub fn NK_get_password_safe_slot_name(slot_number: u8) -> *const c_char;
//     char *NK_get_password_safe_slot_login(uint8_t slot_number);",
    //     char *NK_get_password_safe_slot_password(uint8_t slot_number);",
    pub fn NK_get_password_safe_slot_password(slot_number: u8) -> *const c_char;
    //     char * NK_get_hotp_slot_name(uint8_t slot_number);",
    pub fn NK_get_hotp_slot_name(slot_number: u8) -> *const c_char;
    //     char * NK_get_hotp_code(uint8_t slot_number);",
    pub fn NK_get_hotp_code(slot_number: u8) -> *const c_char;
    pub fn NK_lock_device() -> c_int;
    pub fn NK_logout() -> c_int;
//     int NK_wink();"
}


pub struct Token {
    pub slot_number: u8,
    pub slot_name: String
}

