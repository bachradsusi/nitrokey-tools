use std::collections::HashMap;
use std::ffi::{CString,CStr};
use std::os::raw::c_char;
use std::slice;

use crate::libnitrokey::*;


extern "C" {
    fn getpass(prompt: *const c_char) -> *const c_char;
}

pub fn unlock_passwords() -> bool {
    let get_pass_prompt  = CString::new("Enter your PIN: ").unwrap();
    let pin_str = unsafe {
        getpass(get_pass_prompt.as_ptr())
    };

    let rc = unsafe {
        let mut rc = NK_user_authenticate(pin_str, CString::new("XYXKLM").unwrap().as_ptr());
        if rc != 0 {
            println!("Your PIN is not correct! Please try again. Please be careful to not lock your stick! - err {}", rc);
            let status_str = NK_status();
            println!("{:?}", CStr::from_ptr(status_str));
            false
        } else {
            rc = NK_enable_password_safe(pin_str);
            if rc != 0 {
                println!("Passwords are not available - {}", rc);
                let status_str = NK_status();
                println!("{:?}", CStr::from_ptr(status_str));
                false
            } else {
                true
            }
        }
    };
    rc
}

pub fn get_tokens() -> HashMap<String, u8> {
    let mut tokens: HashMap<String, u8> = HashMap::new();
    let statuses = unsafe {
        let s = NK_get_password_safe_slot_status();
        if s.is_null() {
            return tokens;
        }
        slice::from_raw_parts(s, 16)
    };
    for number in 0..16 {
        if statuses[number] == 1 {
            let slot_name = unsafe {
                let slot_name_str = NK_get_password_safe_slot_name(number as u8);
                CStr::from_ptr(slot_name_str).to_string_lossy().into_owned()
            };
            tokens.insert(slot_name, number as u8);
        }
    };
    tokens
}

pub fn get_token(slot_name: &String) -> String {
    let tokens = get_tokens();
    let token: String = match tokens.get(slot_name) {
        Some(&slot_number) => {
            let slot_code = unsafe {
                let nitro_slot_pass_str = NK_get_password_safe_slot_password(slot_number);
                CStr::from_ptr(nitro_slot_pass_str).to_string_lossy()
            };
            slot_code.into_owned()
        },
        None => {
            String::new()
        }
    };
    token
}
