use std::ffi::CStr;
use std::collections::HashMap;

use crate::libnitrokey::*;

pub fn get_tokens() -> HashMap<String, u8> {
    let mut tokens = HashMap::new();
    for slot_number in 0..15 {
        let slot_name = unsafe {
            let slot_name_str = NK_get_hotp_slot_name(slot_number);
            CStr::from_ptr(slot_name_str).to_string_lossy()
        };

        if slot_name != "" {
            tokens.insert(slot_name.to_string(), slot_number);
        }
    }
    tokens
}

pub fn get_token(slot_name: &String) -> String {
    let tokens = super::get_tokens(super::TokenType::HOTP);
    let token: String = match tokens.get(slot_name) {
        Some(&slot_number) => {
            let slot_code = unsafe {
                let slot_code_str = NK_get_hotp_code(slot_number);
                CStr::from_ptr(slot_code_str).to_string_lossy()
            };
            slot_code.into_owned()
        },
        None => {
            String::new()
        }
    };
    token
}
