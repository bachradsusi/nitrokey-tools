use std::ffi::CStr;
use std::collections::HashMap;
use std::time::{SystemTime, UNIX_EPOCH};

use crate::libnitrokey::*;

pub fn get_tokens() -> HashMap<String, u8> {
    let mut tokens = HashMap::new();
    for slot_number in 0..15 {
        let slot_name = unsafe {
            let slot_name_str = NK_get_totp_slot_name(slot_number);
            CStr::from_ptr(slot_name_str).to_string_lossy()
        };

        if slot_name != "" {
            tokens.insert(slot_name.to_string(), slot_number);
        }
    }
    tokens
}

pub fn get_token(slot_name: &String) -> String {
    let tokens = super::get_tokens(super::TokenType::TOTP);
    let token: String = match tokens.get(slot_name) {
        Some(&slot_number) => {
            let slot_code = unsafe {
                let timestamp = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
                NK_totp_set_time(timestamp.as_secs());
                let slot_code_str = NK_get_totp_code(slot_number, 0, 0, 0);
                CStr::from_ptr(slot_code_str).to_string_lossy()
            };
            slot_code.into_owned()
        },
        None => {
            String::new()
        }
    };
    token
}
