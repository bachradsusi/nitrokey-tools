use std::collections::HashMap;
use std::process::{Command, Stdio};
use std::io::Write;
use std::fmt;

pub mod passwords;
pub mod totp;
pub mod hotp;

pub mod libnitrokey;
use libnitrokey::*;

//#[derive(Clone)]
pub enum TokenType {
    TOTP,
    HOTP,
    PASSWORD
}

impl TokenType {
    fn get_str(&self) -> &str {
        match self {
            TokenType::TOTP => "totp",
            TokenType::HOTP => "hotp",
            TokenType::PASSWORD => "password"
        }
    }
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.get_str())
    }
}

pub enum NitroAction {
    Get,
    List
}

impl NitroAction {
    fn get_str(&self) -> &str {
        match self {
            NitroAction::Get => "get",
            NitroAction::List => "list"
        }
    }
}

impl fmt::Display for NitroAction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.get_str())
    }
}

pub enum NitroError {
    ParseError,
    NoToken
}

// pub fn init_tokens_map<'T>() -> HashMap<&'T str, TokenType> {
//     let tokens_map: HashMap<&str, TokenType> =
//         [("hotp", TokenType::HOTP),
//          ("totp", TokenType::TOTP),
//          ("password", TokenType::PASSWORD)]
//         .iter().cloned().collect();
//     tokens_map
// }

pub fn get_tokens(token_type: TokenType) -> HashMap<String, u8> {
    let _token_size = match token_type {
        TokenType::TOTP => 15,
        TokenType::PASSWORD => 16,
        TokenType::HOTP => 3
    };
    match token_type {
        TokenType::TOTP => {
            totp::get_tokens()
        },
        TokenType::HOTP => {
            hotp::get_tokens()
        },
        TokenType::PASSWORD => {
            passwords::get_tokens()
        }
    }
}

pub fn print_tokens(token_type: TokenType) {
    let tokens = get_tokens(token_type);

    for (slot_name, slot_number) in &tokens {
        println!("{} - {}", slot_number, slot_name);
    }
}

pub fn print_token(token_type: TokenType, slot_name: &String) {
   let token: String = match token_type {
       TokenType::TOTP => {
           totp::get_token(slot_name)
       },
       TokenType::PASSWORD => {
           passwords::get_token(slot_name)
       },
       TokenType::HOTP => {
           hotp::get_token(slot_name)
       }
    };
    println!("{}", token);
    save_to_clipboard(token);
}


pub fn init_libnitrokey() {
    unsafe {
        NK_login_auto();
    }
}

pub fn close_libnitrokey() {
    unsafe {
        NK_lock_device();
        NK_logout()
    };
}

pub fn save_to_clipboard(slot_code: String) {
    let mut child = Command::new("xclip")
        .arg("-selection")
        .arg("clipboard")
        .stdin(Stdio::piped())
        .stdout(Stdio::null())
        .spawn()
        .expect("Failed to spawn xclip child process");

    let xclip_stdin = child.stdin.as_mut().expect("Failed to open stdin");
    xclip_stdin.write_all(slot_code.as_bytes()).expect("Failed to write to stdin");

}
