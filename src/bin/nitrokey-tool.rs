use std::io;

use std::env;

use nitrokey_tools;


fn process_line(line: &String) -> Result<(nitrokey_tools::NitroAction, nitrokey_tools::TokenType, &str), nitrokey_tools::NitroError> {
    let mut line_tokens = line.as_str().split_whitespace();
    let action = match line_tokens.next() {
        Some("get") => {
            nitrokey_tools::NitroAction::Get
        },
        Some("list") => nitrokey_tools::NitroAction::List,
        _ => return Err(nitrokey_tools::NitroError::ParseError)
    };

    let token_type = match line_tokens.next() {
        Some("hotp") => nitrokey_tools::TokenType::HOTP,
        Some("totp") => nitrokey_tools::TokenType::TOTP,
        Some("password") => nitrokey_tools::TokenType::PASSWORD,
        _ => return Err(nitrokey_tools::NitroError::ParseError)
    };

    let slot_name = match line_tokens.next() {
        Some(slot_name) => slot_name,
        None => {
            match action {
                nitrokey_tools::NitroAction::List => "",
                _ => return Err(nitrokey_tools::NitroError::ParseError)
            }
        }
    };

    Ok((action, token_type, slot_name))
}

fn do_action(action: (nitrokey_tools::NitroAction, nitrokey_tools::TokenType, &str)) {
    println!("action - {} - token - {} - slot_name - {}", action.0, action.1, action.2);
    match action.0 {
        nitrokey_tools::NitroAction::Get => nitrokey_tools::print_token(action.1, &String::from(action.2)),
        nitrokey_tools::NitroAction::List => nitrokey_tools::print_tokens(action.1),
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    nitrokey_tools::init_libnitrokey();
    let mut done = false;
    let reader = io::stdin();
    while ! done {
        let mut line = String::new();
        match reader.read_line(&mut line) {
            Ok(n) => {
                if n == 0 {
                    done = true;
                }
                // println!("{} bytes read", n);
                // println!("** {} **", &line);
                match process_line(&line) {
                    Ok(action) => do_action(action),
                    Err(nitrokey_tools::NitroError::ParseError) => println!("parse error"),
                    _ => println!("parse error - should not happen")
                }
            }
            Err(error) => println!("error: {}", error),
        }
        //reader.read_line(line);
        //println!("{}", line);
    }
// 
//     let token_type = nitrokey_tools::TokenType::HOTP;
//     nitrokey_tools::init_libnitrokey();
//     if args.len() < 2 {
//         nitrokey_tools::print_tokens(token_type);
//         return;
//     } else {
//         nitrokey_tools::print_token(token_type, &args[1]);
//     }
//     nitrokey_tools::close_libnitrokey();
}

#[test]
fn first_test() {
    assert!(1 == 1)
}
