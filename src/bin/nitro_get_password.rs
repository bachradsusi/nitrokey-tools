use std::env;

use nitrokey_tools;
use nitrokey_tools::passwords;

fn main() {
    let args: Vec<String> = env::args().collect();
    let token_type = nitrokey_tools::TokenType::PASSWORD;
    nitrokey_tools::init_libnitrokey();
    if !passwords::unlock_passwords() {
        println!("unlock failed");
        return;
    }
    if args.len() < 2 {
        nitrokey_tools::print_tokens(token_type);
        return;
    } else {
        nitrokey_tools::print_token(token_type, &args[1]);
    }
    nitrokey_tools::close_libnitrokey();
}

#[test]
fn first_test() {
    assert!(1 == 1)
}
