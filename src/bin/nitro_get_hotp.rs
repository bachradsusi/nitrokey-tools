use std::env;

use nitrokey_tools;

fn main() {
    let args: Vec<String> = env::args().collect();
    let token_type = nitrokey_tools::TokenType::HOTP;
    nitrokey_tools::init_libnitrokey();
    if args.len() < 2 {
        nitrokey_tools::print_tokens(token_type);
        return;
    } else {
        nitrokey_tools::print_token(token_type, &args[1]);
    }
    nitrokey_tools::close_libnitrokey();
}

#[test]
fn first_test() {
    assert!(1 == 1)
}
